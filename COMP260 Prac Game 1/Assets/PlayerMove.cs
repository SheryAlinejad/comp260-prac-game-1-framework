﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public string HorizontalInput;
	public string VerticalInput;
	public float maxSpeed = 10.0f; // in metres per second
	public float acceleration = 1.0f; // in metres/second/second
	private float speed = 0.1f;    // in metres/second
	public float brake = 0.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second

	// Use this for initialization
	void Start () {
	}
		
	public Vector2 velocity;
	// in metres per second

	// Update is called once per frame
	void Update () {
		// the horizontal axis controls the turn
		float turn = Input.GetAxis(HorizontalInput);

		// turn the car
		transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(VerticalInput);

		if (forwards == 0.0f) {
			speed = 0.0f;
		} else if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} else {
			// braking
			if (speed == 0.0f) {
				speed = 0.0f;
			} else if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {
				speed = speed + brake * Time.deltaTime;
			}
		}
		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector3.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed,maxSpeed);

		// move the object
		transform.Translate(velocity * Time.deltaTime);

	}
}
